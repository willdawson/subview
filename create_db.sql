create table sv_user (
  id int not null primary key generated always as identity (start with 1, increment by 1),
  utsId varchar(255),
  name varchar(255) not null,
  email varchar(255) not null,
  password varchar(255) not null
);

create table sv_subject (
  id int not null primary key generated always as identity (start with 1, increment by 1),
  utsId varchar(255) not null,
  name varchar(255) not null,
  abbreviation varchar(255),
  description long varchar
);

create table sv_review (
  id int not null primary key generated always as identity (start with 1, increment by 1),
  title long varchar,
  text long varchar not null,
  userId int not null,
  subjectId int not null,
  foreign key (userId) references sv_user(id),
  foreign key (subjectId) references sv_subject(id)
);

insert into sv_user (utsId, name, email, password) values
  ('11656713', 'Will Dawson', 'William.H.Dawson@student.uts.edu.au', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),
  ('12345678', 'Jane Doe', 'Jane.Doe@student.uts.edu.au', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');

insert into sv_subject (utsId, name, abbreviation, description) values
  ('31242', 'Advanced Internet Programming', 'AIP', 'Description!'),
  ('31285', 'Mobile Applications Development', 'MAD', '');

insert into sv_review (title, text, userId, subjectId) values
  ('Full on but great!', 'I learnt a lot of Java web concepts, and the tutor was great.', 1, 1),
  ('Well Designed', 'Labs were well structured, and knowledge learning was fast.', 2, 2);

create view jdbcrealm_user (username, password) as
  select utsId, password
  from sv_user;

create view jdbcrealm_group (username, groupname) as
  select utsId, 'Users'
  from sv_user;