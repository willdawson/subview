/**
 * Definitions
 */
var gulp         = require('gulp'),
    notify       = require('gulp-notify'),
    sass         = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer');

var sass_dir = 'src/main/web/styles/';

/**
 * Gulp.js Default
 */
gulp.task('default', ['sass'], function() {
    gulp.watch(sass_dir + '**/*.scss', ['sass']);
});

/**
 * SASS Processing
 */
gulp.task('sass', function() {
    return sass(sass_dir + 'main.scss', {style: 'compressed'})
        .on('error', function(err) {
            console.error('Error', err.message);
            notify.onError().apply(this, arguments);
            this.emit('end');
        })
        .pipe(autoprefixer())
        .pipe(gulp.dest(sass_dir))
});