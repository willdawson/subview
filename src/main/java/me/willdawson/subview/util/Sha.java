package me.willdawson.subview.util;

import java.security.*;

/**
 * Converts a plain text password to a SHA-256 encrypted password.
 * Source: https://gist.github.com/avilches/750151
 */
public class Sha {

    /**
     * Generate a SHA256 encrypted string.

     * @param data
     * @return the encrypted string.
     * @throws NoSuchAlgorithmException
     */
    public static String hash256(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(data.getBytes());
        return bytesToHex(md.digest());
    }

    /**
     * Convert the specified bytes to a hexadecimal value.
     *
     * @param bytes
     * @return the hex string.
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();

        for (byte byt : bytes) {
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        }

        return result.toString();
    }

}