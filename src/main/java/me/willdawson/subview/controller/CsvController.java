package me.willdawson.subview.controller;

import me.willdawson.subview.csv.CsvReview;
import me.willdawson.subview.dao.ReviewDAO;
import me.willdawson.subview.model.Review;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Controller used to export the CSV data from the interface.
 */

@Named
@RequestScoped
public class CsvController {

    /**
     * Gets a CSV StringBuffer for the Review model, and outputs it into the response as 'reviews.csv'.
     */
    public void export() {
        ArrayList<Review> reviews = new ReviewDAO().findAll();
        CsvReview csv = new CsvReview(reviews);
        StringBuffer buffer = csv.export();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

        response.reset();
        response.setContentLength(buffer.length());
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"reviews.csv\"");

        try {
            OutputStream out = response.getOutputStream();
            out.write(buffer.toString().getBytes());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

}
