package me.willdawson.subview.controller;

import me.willdawson.subview.dao.ReviewDAO;
import me.willdawson.subview.dao.SubjectDAO;
import me.willdawson.subview.model.Review;
import me.willdawson.subview.model.Subject;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;

@Named
@RequestScoped
public class ReviewController extends BaseController {

    private Review review;

    public ReviewController() {
        review = new Review();
    }

    public Review getReview() {
        return review;
    }

    /**
     * Retrieve all subjects.
     * @return
     */
    public ArrayList<Subject> getSubjects() {
        return new SubjectDAO().findAll();
    }

    /**
     * Initialise the view request.
     *
     * @param id
     * @return
     */
    public String initView(int id) {
        review = new ReviewDAO().find(id);

        if (review == null) {
            showError("Sorry, that review doesn't exist.");
            return redirect("/index");
        }

        return null;
    }

    /**
     * Initialise the edit request.
     *
     * @param id
     * @return
     */
    public String initEdit(int id) {
        review = new ReviewDAO().find(id);

        if (review == null) {
            showError("Sorry, that review doesn't exist.");
            return redirect("/index");
        }

        // Only authors can edit reviews.
        if (review.getUserId() != getAuthUser().getId()) {
            showError("You do not have permission to edit that review.");
            return redirect("/index");
        }

        return null;
    }

    /**
     * Initialise the delete request.
     *
     * @param id
     * @return
     */
    public String initDelete(int id) {
        review = new ReviewDAO().find(id);

        if (review == null) {
            showError("Sorry, that review doesn't exist.");
            return redirect("/index");
        }

        // Only authors can delete reviews.
        if (review.getUserId() != getAuthUser().getId()) {
            showError("You do not have permission to delete that review.");
            return redirect("/index");
        }

        return null;
    }

    /**
     * Create a new review in storage.
     */
    public String create() {
        review.setUserId(getAuthUser().getId());

        int id = new ReviewDAO().create(this.review);

        return redirect("view?id=" + id);
    }

    /**
     * Update an existing review in storage.
     */
    public String edit() {
        new ReviewDAO().update(review);

        return redirect("view?includeViewParams=true");
    }

    /**
     * Delete an existing review in storage.
     */
    public String delete() {
        new ReviewDAO().delete(review);

        return redirect("/index");
    }

}
