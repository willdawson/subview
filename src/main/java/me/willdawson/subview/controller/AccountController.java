package me.willdawson.subview.controller;

import me.willdawson.subview.dao.UserDAO;
import me.willdawson.subview.model.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Controller used to create and update user accounts.
 */

@Named
@RequestScoped
public class AccountController extends BaseController {

    private User user;

    public AccountController() {
        user = new User();
    }

    public User getUser() {
        return user;
    }

    /**
     * Initialise the user object if necessary.
     *
     * @param id of the user to initialise.
     */
    public void init(int id) {
        user = new UserDAO().find(id);
    }

    /**
     * Update the user's details.
     *
     * @return the redirect url.
     */
    public String update() {
        new UserDAO().update(user);

        return redirect("account");
    }

    /**
     * Create a new user, which we use to register new users.
     *
     * @return the redirect url.
     */
    public String create() {
        new UserDAO().create(user);

        return redirect("login");
    }

}
