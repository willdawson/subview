package me.willdawson.subview.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.ServletException;

/**
 * Controller used to login and logout users.
 */

@Named
@RequestScoped
public class AuthController extends BaseController {

    private Account account;

    public AuthController() {
        account = new Account();
    }

    public Account getAccount() {
        return account;
    }

    /**
     * Will use the entered credentials and attempt to log in. On success, the user will be taken to
     * the home page. If the attempt fails, an error message will be shown.
     *
     * @return The URL of the intended next page.
     */
    public String login() {
        try {
            getRequest().login(account.getUsername(), account.getPassword());
        } catch (ServletException e) {
            String message;

            if (e.getMessage().equals("Login failed")) {
                message = "Your username or password was incorrect. Please try again.";
            } else {
                message = e.getMessage() + ".";
            }

            showError(message);
            return null;
        }

        return redirect("/index");
    }

    /**
     * Attempts to log the user out, and redirects to the home page.
     *
     * @return The URL of the intended next page.
     * @throws ServletException if the user was not authenticated.
     */
    public String logout() throws ServletException {
        getRequest().logout();

        showSuccess("You have been successfully logged out.");

        return redirect("/index");
    }

    /**
     * Account class to temporarily store the current requests account object.
     */
    public class Account {
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

}
