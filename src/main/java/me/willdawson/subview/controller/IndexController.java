package me.willdawson.subview.controller;

import me.willdawson.subview.dao.ReviewDAO;
import me.willdawson.subview.dao.SubjectDAO;
import me.willdawson.subview.model.Review;
import me.willdawson.subview.model.Subject;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;

/**
 * Homepage controller used to list subjects and reviews.
 */

@Named
@RequestScoped
public class IndexController extends BaseController {

    /**
     * Retrieve all subjects from the database.
     *
     * @return all subjects.
     */
    public ArrayList<Subject> getSubjects() {
        return new SubjectDAO().findAll();
    }

    /**
     * Retrieve all reviews from the database.
     *
     * @return all reviews.
     */
    public ArrayList<Review> getReviews() {
        return new ReviewDAO().findAll();
    }

}
