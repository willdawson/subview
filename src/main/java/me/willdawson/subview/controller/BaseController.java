package me.willdawson.subview.controller;

import me.willdawson.subview.dao.UserDAO;
import me.willdawson.subview.model.User;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 * BaseController should be used to extend all controllers. It provides a number of
 * helpful methods.
 */

@Named
@RequestScoped
public class BaseController {

    private User auth;

    public BaseController() {
        auth = getAuthUser();
    }

    protected HttpServletRequest getRequest() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (HttpServletRequest) context.getExternalContext().getRequest();
    }

    /**
     * A nicer way to handle faces redirect URLs.
     *
     * @param url
     * @return
     */
    protected String redirect(String url) {
        url = (url.indexOf("?") == -1) ? url + "?" : url + "&";
        return url + "faces-redirect=true";
    }

    /**
     * Shows a FacesMessage with no severity level.
     *
     * @param message
     */
    protected void showSuccess(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    /**
     * Shows a FacesMessage with a level of SEVERITY_ERROR.
     *
     * @param message
     */
    protected void showError(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    /**
     * Get the authenticated user object.
     *
     * @return The authenticated user.
     */
    public User getAuthUser() {
        if (auth == null) {
            String username = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
            auth = new UserDAO().findByUtsId(username);
        }

        return auth;
    }

}
