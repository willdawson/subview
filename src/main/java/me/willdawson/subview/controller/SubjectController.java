package me.willdawson.subview.controller;

import me.willdawson.subview.dao.ReviewDAO;
import me.willdawson.subview.dao.SubjectDAO;
import me.willdawson.subview.model.Review;
import me.willdawson.subview.model.Subject;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;

/**
 * Controller used to view all reviews for a subject.
 */

@Named
@RequestScoped
public class SubjectController extends BaseController {

    private Subject subject;

    public SubjectController() {
        subject = new Subject();
    }

    /**
     * Initialise the current subject, and show an error if necessary.
     *
     * @param id
     * @return the redirect url.
     */
    public String initView(int id) {
        subject = new SubjectDAO().find(id);

        if (subject == null) {
            showError("Sorry, that subject doesn't exist.");
            return redirect("/index");
        }

        return null;
    }

    public Subject getSubject() {
        return subject;
    }

    /**
     * Return all reviews for the current subject.
     *
     * @return the reviews.
     */
    public ArrayList<Review> getReviews() {
        return new ReviewDAO().findBySubjectId(subject.getId());
    }

}
