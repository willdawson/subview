package me.willdawson.subview.dao;

import me.willdawson.subview.model.User;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Data Access Object implementation for the user class.
 * TODO: Create a DAO interface, then implement.
 */
public class UserDAO {

    private static final String JNDI_NAME  = "jdbc/subview";
    private static final String SELECT     = "select * from sv_user";
    private static final String SELECT_ALL = SELECT;
    private static final String SELECT_ID  = SELECT + " where id = ?";
    private static final String SELECT_UTS = SELECT + " where utsId = ?";

    private static final String INSERT_ALL = "insert into sv_user " +
                                             "(utsId, name, email, password) values " +
                                             "(?, ?, ?, ?)";

    private static final String UPDATE_ALL = "update sv_user " +
                                             "set name=?, password=? " +
                                             "where id = ?";

    private DataSource dataSource;

    public UserDAO() {
        try {
            dataSource = InitialContext.doLookup(JNDI_NAME);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Populate a user object based on a given result set.
     *
     * @param result
     * @return the user object.
     * @throws SQLException
     */
    public User initDTO(ResultSet result) throws SQLException {
        return new User(
            result.getInt("id"),
            result.getString("utsId"),
            result.getString("name"),
            result.getString("email"),
            result.getString("password")
        );
    }

    /**
     * Find all users in the database.
     *
     * @return the users.
     */
    public ArrayList<User> findAll() {
        ArrayList<User> users = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(SELECT_ALL)) {

            while (result.next()) {
                users.add(initDTO(result));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    /**
     * Find the user relating to the specified uts id.
     *
     * @param utsId
     * @return the user.
     */
    public User findByUtsId(String utsId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(SELECT_UTS)) {

            prepared.setString(1, utsId);

            try (ResultSet result = prepared.executeQuery()) {
                if (result.next()) return initDTO(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Finds the specified user in the database.
     *
     * @param id of the user.
     * @return the user.
     */
    public User find(int id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(SELECT_ID)) {

            prepared.setInt(1, id);

            try (ResultSet result = prepared.executeQuery()) {
                if (result.next()) return initDTO(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Inserts the user into the database.
     *
     * @param user
     * @return The generated ID of the row.
     */
    public void create(User user) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(INSERT_ALL)) {

            prepared.setString(1, user.getUtsId());
            prepared.setString(2, user.getName());
            prepared.setString(3, user.getEmail());
            prepared.setString(4, user.getPassword());

            prepared.execute();

            if (prepared.getUpdateCount() != 1) {
                Logger log = Logger.getLogger(this.getClass().getName());
                log.warning("Failed to add user to database");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User creation failed. Please try again."));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the specified user in the database.
     *
     * @param user
     */
    public void update(User user) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(UPDATE_ALL)) {

            prepared.setString(1, user.getName());
            prepared.setString(2, user.getPassword());
            prepared.setInt(3, user.getId());

            prepared.execute();

            if (prepared.getUpdateCount() != 1) {
                Logger log = Logger.getLogger(this.getClass().getName());
                log.warning("Failed to update user record.");

                // TODO: Abstract this out into presentation logic.
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User update failed. Please try again."));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
