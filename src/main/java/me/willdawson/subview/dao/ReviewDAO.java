package me.willdawson.subview.dao;

import me.willdawson.subview.model.Review;
import me.willdawson.subview.model.User;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Data Access Object implementation for the review class.
 * TODO: Create a DAO interface, then implement.
 */
public class ReviewDAO {

    private static final String JNDI_NAME  = "jdbc/subview";
    private static final String SELECT     = "select * from sv_review";
    private static final String SELECT_ALL = SELECT;
    private static final String SELECT_ID  = SELECT + " where id = ?";
    private static final String SELECT_SUB = SELECT + " where subjectId = ?";

    private static final String INSERT_ALL = "insert into sv_review " +
                                             "(title, text, subjectId, userId) values " +
                                             "(?, ?, ?, ?)";

    private static final String UPDATE_ALL = "update sv_review " +
                                             "set title=?, text=? " +
                                             "where id = ?";

    private static final String DELETE     = "delete from sv_review " +
                                             "where id = ?";

    private DataSource dataSource;

    public ReviewDAO() {
        try {
            dataSource = InitialContext.doLookup(JNDI_NAME);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Populate a review object based on a given result set.
     *
     * @param result
     * @return the review object.
     * @throws SQLException
     */
    public Review initDTO(ResultSet result) throws SQLException {
        return new Review(
            result.getInt("id"),
            result.getString("title"),
            result.getString("text"),
            result.getInt("userId"),
            result.getInt("subjectId")
        );
    }

    /**
     * Find all reviews in the database.
     *
     * @return the reviews.
     */
    public ArrayList<Review> findAll() {
        ArrayList<Review> reviews = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(SELECT_ALL)) {

            while (result.next()) {
                reviews.add(initDTO(result));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return reviews;
    }

    /**
     * Find the reviews relating to the specified subject id.
     *
     * @param id
     * @return the reviews.
     */
    public ArrayList<Review> findBySubjectId(int id) {
        ArrayList<Review> reviews = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(SELECT_SUB)) {

            prepared.setInt(1, id);

            ResultSet result = prepared.executeQuery();

            while (result.next()) {
                reviews.add(initDTO(result));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return reviews;
    }

    /**
     * Finds the specified review in the database.
     *
     * @param id of the review.
     * @return the review.
     */
    public Review find(int id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(SELECT_ID)) {

            prepared.setInt(1, id);

            try (ResultSet result = prepared.executeQuery()) {
                if (result.next()) return initDTO(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Inserts the review into the database.
     *
     * @param review
     * @return The generated ID of the row.
     */
    public int create(Review review) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(INSERT_ALL,
                     new String[] { "id" })) {

            prepared.setString(1, review.getTitle());
            prepared.setString(2, review.getText());
            prepared.setInt(3, review.getSubjectId());
            prepared.setInt(4, review.getUserId());

            prepared.executeUpdate();

            ResultSet result = prepared.getGeneratedKeys();

            if (result.next()) {
                return result.getInt(1);
            } else {
                Logger log = Logger.getLogger(this.getClass().getName());
                log.warning("Failed to add user to database");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User creation failed. Please try again."));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * Update the specified review in the database.
     *
     * @param review
     */
    public void update(Review review) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(UPDATE_ALL)) {

            prepared.setString(1, review.getTitle());
            prepared.setString(2, review.getText());
            prepared.setInt(3, review.getId());

            prepared.executeUpdate();

            if (prepared.getUpdateCount() != 1) {
                Logger log = Logger.getLogger(this.getClass().getName());
                log.warning("Failed to update user record.");

                // TODO: Abstract this out into presentation logic.
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User update failed. Please try again."));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete the specified review from the database.
     *
     * @param review
     */
    public void delete(Review review) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(DELETE)) {

            prepared.setInt(1, review.getId());

            prepared.execute();

            if (prepared.getUpdateCount() != 1) {
                Logger log = Logger.getLogger(this.getClass().getName());
                log.warning("Failed to delete user record.");

                // TODO: Abstract this out into presentation logic.
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User delete failed. Please try again."));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
