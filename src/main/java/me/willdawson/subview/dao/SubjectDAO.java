package me.willdawson.subview.dao;

import me.willdawson.subview.model.Subject;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

/**
 * Data Access Object implementation for the subject class.
 * TODO: Create a DAO interface, then implement.
 */
public class SubjectDAO {

    private static final String JNDI_NAME  = "jdbc/subview";
    private static final String SELECT     = "select * from sv_subject";
    private static final String SELECT_ALL = SELECT;
    private static final String SELECT_ID  = SELECT + " where id = ?";

    private DataSource dataSource;

    public SubjectDAO() {
        try {
            dataSource = InitialContext.doLookup(JNDI_NAME);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Populate a subject object based on a given result set.
     *
     * @param result
     * @return the subject object.
     * @throws SQLException
     */
    public Subject initDTO(ResultSet result) throws SQLException {
        return new Subject(
            result.getInt("id"),
            result.getString("utsId"),
            result.getString("name"),
            result.getString("abbreviation"),
            result.getString("description")
        );
    }

    /**
     * Find all subjects in the database.
     *
     * @return the subjects.
     */
    public ArrayList<Subject> findAll() {
        ArrayList<Subject> subjects = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(SELECT_ALL)) {

            while (result.next()) {
                subjects.add(initDTO(result));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return subjects;
    }

    /**
     * Finds the specified subject in the database.
     *
     * @param id of the subject.
     * @return the subject.
     */
    public Subject find(int id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement prepared = connection.prepareStatement(SELECT_ID)) {

            prepared.setInt(1, id);

            try (ResultSet result = prepared.executeQuery()) {
                if (result.next()) return initDTO(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
