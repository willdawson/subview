package me.willdawson.subview.model;

import javax.validation.constraints.Size;

/**
 * Object describing a Review.
 * All methods are trivial getters and setters.
 */
public class Review {
    private int id;
    private String title;
    private String text;
    private int userId;
    private int subjectId;

    public Review() {}

    public Review(int id, String title, String text, int userId, int subjectId) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.userId = userId;
        this.subjectId = subjectId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Size(min=1, max=100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Size(min=30)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
