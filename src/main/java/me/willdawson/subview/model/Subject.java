package me.willdawson.subview.model;

import javax.validation.constraints.Size;

/**
 * Object describing a Subject.
 * All methods are trivial getters and setters.
 */
public class Subject {
    private int id;
    private String utsId;
    private String name;
    private String abbreviation;
    private String description;

    public Subject() {}

    public Subject(int id, String utsId, String name, String abbreviation, String description) {
        this.id = id;
        this.utsId = utsId;
        this.name = name;
        this.abbreviation = abbreviation;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    @Size(min=6, max=8)
    public String getUtsId() {
        return utsId;
    }

    public void setUtsId(String utsId) {
        this.utsId = utsId;
    }

    @Size(min=1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Size(min=1, max=5)
    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Size(min=140)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
