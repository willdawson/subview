package me.willdawson.subview.model;

import me.willdawson.subview.util.Sha;

import javax.validation.constraints.Size;
import java.security.NoSuchAlgorithmException;

/**
 * Object describing a User.
 * All methods are trivial getters and setters.
 */
public class User {
    private int id;
    private String utsId;
    private String name;
    private String email;
    private String password;

    public User() {}

    public User(int id, String utsId, String name, String email, String password) {
        this.id = id;
        this.utsId = utsId;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Size(min=6, max=8)
    public String getUtsId() {
        return utsId;
    }

    public void setUtsId(String utsId) {
        this.utsId = utsId;
    }

    @Size(min=1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Size(min=1)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Size(min=1)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) throws NoSuchAlgorithmException {
        this.password = Sha.hash256(password);
    }
}
