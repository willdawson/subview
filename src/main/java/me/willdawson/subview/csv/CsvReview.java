package me.willdawson.subview.csv;

import me.willdawson.subview.model.Review;

import java.util.ArrayList;

/**
 * Implementation of the CSV class for the Review model.
 */
public class CsvReview extends CsvModel<Review> {

    public CsvReview(ArrayList<Review> data) {
        super(data);
    }

    @Override
    protected String getHeaders() {
        return "ID, Title, Text, User ID, Subject ID";
    }

    @Override
    protected String getRow(Review review) {
        String row = "";
        row += review.getId() + ", ";
        row += "\"" + review.getTitle() + "\", ";
        row += "\"" + review.getText() + "\", ";
        row += review.getUserId() + ", ";
        row += review.getSubjectId();
        return row;
    }

}
