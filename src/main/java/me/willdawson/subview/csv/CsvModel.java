package me.willdawson.subview.csv;

import java.util.ArrayList;

/**
 * Interface used to output a CSV representation of a specified model, T.
 * @param <T>
 */
public abstract class CsvModel<T> {

    private ArrayList<T> data;

    protected CsvModel(ArrayList<T> data) {
        this.data = data;
    }

    /**
     * Get the string describing the models headers.
     *
     * @return the string.
     */
    protected abstract String getHeaders();

    /**
     * Get the string describing one row of the model.
     *
     * @param model to describe.
     * @return the string.
     */
    protected abstract String getRow(T model);

    /**
     * Export the CSV text for the current instantiated model.
     * @return
     */
    public StringBuffer export() {
        StringBuffer out = new StringBuffer();
        out.append(getHeaders() + "\n");

        for (T model : data) {
            out.append(getRow(model) + "\n");
        }

        return out;
    }

}
